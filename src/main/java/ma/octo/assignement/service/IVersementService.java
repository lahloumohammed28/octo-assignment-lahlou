package ma.octo.assignement.service;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface IVersementService {
    List<VersementDto> loadAll();
    void saveVersement(Versement versement);
    void saveVersementDto(VersementDto versementDto) throws CompteNonExistantException, TransactionException;
}
