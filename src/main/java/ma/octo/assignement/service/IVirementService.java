package ma.octo.assignement.service;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface IVirementService {
    List<VirementDto> loadAll();
    //void saveVirement(Virement virement) throws CompteNonExistantException, TransactionException;
    void saveVirementDto(VirementDto virementDto) throws CompteNonExistantException, TransactionException;
}
