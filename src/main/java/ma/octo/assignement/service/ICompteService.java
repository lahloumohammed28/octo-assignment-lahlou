package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;

import java.util.List;

public interface ICompteService {
    List<Compte> loadAll();
    void saveCompte(Compte compte);
    Compte getCompte(Long id);
    Compte findByNrCompte(String numeroCompte);
    Compte findByRib(String rib);
    void deleteCompte(Long id);

}
