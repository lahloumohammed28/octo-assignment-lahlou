package ma.octo.assignement.service;

import ma.octo.assignement.domain.AuditTransfertArgent;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditTransfertArgentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AutiService {

    Logger LOGGER = LoggerFactory.getLogger(AutiService.class);

    @Autowired
    private AuditTransfertArgentRepository auditTransfertArgentRepository;

    public void setAuditTransfertArgentVirement(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.VIREMENT);

        AuditTransfertArgent audit = new AuditTransfertArgent();
        audit.setEventType(EventType.VIREMENT);
        audit.setMessage(message);
        auditTransfertArgentRepository.save(audit);
    }


    public void setAuditTransfertArgentVersement(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.VERSEMENT);

        AuditTransfertArgent audit = new AuditTransfertArgent();
        audit.setEventType(EventType.VERSEMENT);
        audit.setMessage(message);
        auditTransfertArgentRepository.save(audit);
    }
}
