package ma.octo.assignement.repository;

import ma.octo.assignement.domain.AuditTransfertArgent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuditTransfertArgentRepository extends JpaRepository<AuditTransfertArgent, Long> {
}
