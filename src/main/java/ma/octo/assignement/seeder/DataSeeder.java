package ma.octo.assignement.seeder;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.IUtilisateurService;
import ma.octo.assignement.service.IVersementService;
import ma.octo.assignement.service.IVirementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

@Component
public class DataSeeder implements CommandLineRunner {

    @Autowired
    private ICompteService compteService;
    @Autowired
    private IUtilisateurService utilisateurService;
    @Autowired
    private IVirementService virementService;
    @Autowired
    private IVersementService versementService;

    @Override
    public void run(String... args)  {
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setLastname("Lahlou");
        utilisateur1.setFirstname("Mohammed");
        utilisateur1.setGender(Gender.M);

        utilisateurService.saveUtilisateur(utilisateur1);


        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setLastname("Lahlou");
        utilisateur2.setFirstname("Ghita");
        utilisateur2.setGender(Gender.F);

        utilisateurService.saveUtilisateur(utilisateur2);

        Compte compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("123456010000A00000100012");
        compte1.setSolde(200000);
        compte1.setUtilisateur(utilisateur1);
        compte1.setDateCreation(new Date());

        compteService.saveCompte(compte1);

        Compte compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("123456010000B02500100012");
        compte2.setSolde(140000);
        compte2.setUtilisateur(utilisateur2);
        compte2.setDateCreation(new Date());

        compteService.saveCompte(compte2);

        Virement virement = new Virement();
        virement.setMontantVirement(3000);
        virement.setCompteBeneficiaire(compte2);
        virement.setCompteEmetteur(compte1);
        virement.setDateExecution(new Date());
        virement.setMotifVirement("Assignment 2021");



        VirementDto virementDto = VirementMapper.map(virement);

        try {
            virementService.saveVirementDto(virementDto);
        }catch(Exception e){

        }

        Versement versement = new Versement();
        versement.setMontantVirement(500);
        versement.setCompteBeneficiaire(compte1);
        versement.setDateExecution(new Date());

        VersementDto versementDto = VersementMapper.map(versement);
        try {
            versementService.saveVersementDto(versementDto);
        }catch(Exception e){

        }
    }
}
