package ma.octo.assignement.domain;

import ma.octo.assignement.domain.util.Gender;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class UtilisateurTest {
    @Test
    public void shouldGetUtilisateurProperties() throws Exception {
        Long id = 1L;
        Gender gender = Gender.F;
        String lastName = "Lahlou";
        String firstName = "Mohammed";
        Date birthDate = null;

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setId(id);
        utilisateur.setGender(gender);
        utilisateur.setLastname(lastName);
        utilisateur.setFirstname(firstName);
        utilisateur.setBirthdate(birthDate);

        Assertions.assertTrue(utilisateur.getId() == id);
        Assertions.assertTrue(utilisateur.getGender() == gender);
        Assertions.assertTrue(utilisateur.getLastname() == lastName);
        Assertions.assertTrue(utilisateur.getFirstname() == firstName);
        Assertions.assertTrue(utilisateur.getBirthdate() == birthDate);

    }
}
