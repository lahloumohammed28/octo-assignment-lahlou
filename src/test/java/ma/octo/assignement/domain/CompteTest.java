package ma.octo.assignement.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.Date;

public class CompteTest {

    @Test
    public void shouldGetCompteProperties() throws Exception {

        Long id = 1L;
        double solde = 2000;
        String nrCompte = "1234567891234567";
        Utilisateur utilisateur = new Utilisateur();
        String rib = "123456123456789123456712";
        Date date = new Date();



        Compte compte = new Compte();
        compte.setId(id);
        compte.setSolde(solde);
        compte.setNrCompte(nrCompte);
        compte.setUtilisateur(utilisateur);
        compte.setRib(rib);
        compte.setDateCreation(date);

        Assertions.assertTrue(compte.getId() == id);
        Assertions.assertTrue(compte.getSolde() == solde);
        Assertions.assertTrue(compte.getNrCompte() == nrCompte);
        Assertions.assertTrue(compte.getUtilisateur() == utilisateur);
        Assertions.assertTrue(compte.getRib() == rib);
        Assertions.assertTrue(compte.getDateCreation() == date);
    }

}
